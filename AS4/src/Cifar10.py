
import numpy as np
import os
import pickle
import random

from keras import models
from keras import layers
from keras import optimizers
from keras import regularizers
from keras.utils import to_categorical
from keras.applications.inception_v3 import InceptionV3
from keras import Input,Model
def unpickle(file):
    with open(file, 'rb') as fo:
        d = pickle.load(fo, encoding='bytes')
    return d
def PlotGraph(history):
    import matplotlib.pyplot as plt
    acc = history.history['acc']
    val_acc = history.history['val_acc']
    loss = history.history['loss']
    val_loss = history.history['val_loss']
    epochs = range(1,len(acc) + 1)

#Validation vs Training in accuracy
    plt.plot(epochs,acc,'bo',label='training accuracy')
    plt.plot(epochs,val_acc,'b',label='Validation accuracy')
    plt.xlabel('Epochs')
    plt.ylabel('Accuracy')
    plt.legend()
    plt.figure()

    plt.plot(epochs,loss,'bo',label='training loss')
    plt.plot(epochs,val_loss,'b',label='Validation loss')
    plt.xlabel('Epochs')
    plt.ylabel('Loss')
    plt.legend()
    plt.figure()
    plt.show()

def read_data(folder):
    x_data_temp = []
    y_data_temp = []
    x_test_data_temp = []
    y_test_data_temp = []
    # We don't use numpy's vstack here as that would be wasteful, because every time you do a vstack, numpy would end
    # up copying the whole array to a new location. Hence we use a little trick to first store the data in a list and
    # then convert it to an numpy array

    for file in os.listdir(folder):
        if file.endswith(".meta") or file.endswith(".html"):
            print("Ignoring html and meta files")
        elif "test_batch" in file:
            # test data file detected. we are gonna load it separately
            test_data_temp = unpickle(folder + "/" + file)
            x_test_data_temp.append(test_data_temp[b'data'])
            y_test_data_temp.append(test_data_temp[b'labels'])
        else:
            temp_data = unpickle(folder + "/" + file)
            x_data_temp.append(temp_data[b'data'])
            y_data_temp.append(temp_data[b'labels'])
    x_data = np.array(x_data_temp)
    y_data = np.array(y_data_temp)
    x_test_data = np.array(x_test_data_temp)
    y_test_data = np.array(y_test_data_temp)
    return [x_data, y_data, x_test_data, y_test_data]


fp = './cifar-10-batches-py'
X_train_temp, y_train_temp, X_test_temp, y_test_temp = read_data(fp)


# Reshaping it Since it contains extra dimensions

X_train = X_train_temp.reshape(X_train_temp.shape[0] * X_train_temp.shape[1], X_train_temp.shape[2])
y_train = y_train_temp.reshape(y_train_temp.shape[0] * y_train_temp.shape[1])
X_test = X_test_temp.reshape(X_test_temp.shape[0] * X_test_temp.shape[1], X_test_temp.shape[2])
y_test = y_test_temp.reshape(y_test_temp.shape[0] * y_test_temp.shape[1]) 


from sklearn.model_selection import train_test_split 

X_train, X_val, y_train, y_val = train_test_split(X_train, y_train, test_size=0.2, random_state=4)


#Reshaping it into 32 * 32 * 3 image slices

nb_classes = 10
img_rows, img_col = 32, 32
img_channels = 3
nb_filters = 32
nb_pool = 2
nb_conv = 3

X_train = X_train.reshape(X_train.shape[0], 32, 32, 3)


X_val = X_val.reshape(X_val.shape[0], 32, 32, 3)


X_test = X_test.reshape(X_test.shape[0], 32, 32, 3)

#Normalizing the dataset
X_train = X_train.astype('float32')/255
X_val = X_val.astype('float32')/255
y_train = y_train.astype('float32')
y_val = y_val.astype('float32')
X_test = X_test.astype('float32')/255
y_test =  y_test.astype('float32')
y_train_encoded = to_categorical(y_train)
y_val_encoded = to_categorical(y_val)
y_test_encoded = to_categorical(y_test)


#Adding our model to restnet block

input_tensor = Input(shape=(32,32,3))
block1 = layers.Conv2D(32,3,kernel_regularizer=regularizers.l2(0.001),activation='relu',padding='same')(input_tensor)
block1 = layers.MaxPooling2D(2)(block1)
block1 = layers.Dropout(0.2)(block1)
block1 = layers.BatchNormalization()(block1)

block2 = layers.Conv2D(32,3,kernel_regularizer=regularizers.l2(0.001),activation='relu',padding='same')(block1)
#block2 = layers.MaxPooling2D(2)(block1)
block2 = layers.Dropout(0.2)(block2)
block2 = layers.BatchNormalization()(block2)

#Adding restnet connection

rest1 = layers.add([block2,block1])

block3 = layers.Conv2D(64,3,kernel_regularizer=regularizers.l2(0.001),activation='relu',padding='same')(rest1)
block3 = layers.MaxPooling2D(2)(block3)
block3 = layers.Dropout(0.2)(block3)
block3 = layers.BatchNormalization()(block3)

block4 = layers.Conv2D(64,3,kernel_regularizer=regularizers.l2(0.001),activation='relu',padding='same')(block3)
#block2 = layers.MaxPooling2D(2)(block1)
block4 = layers.Dropout(0.2)(block4)
block4 = layers.BatchNormalization()(block4)

rest2 = layers.add([block4,block3])

block5 = layers.Conv2D(64,3,kernel_regularizer=regularizers.l2(0.001),activation='relu',padding='same')(rest2)
block5 = layers.Dropout(0.2)(block5)
block5 = layers.BatchNormalization()(block5)

block5 = layers.Flatten()(block5)
block5 = layers.Dropout(0.2)(block5)
output_dense = layers.Dense(128,kernel_regularizer=regularizers.l2(0.001),activation='relu')(block5)
output_dense = layers.Dropout(0.2)(output_dense)
output_dense = layers.BatchNormalization()(output_dense)
output = layers.Dense(10,activation='softmax')(output_dense)
RestnetModel = Model(input_tensor,output)
RestnetModel.summary()
RestnetModel.compile(optimizer=optimizers.Adam(lr=1e-4),loss='categorical_crossentropy',metrics=['acc'])
history = RestnetModel.fit(X_train,y_train_encoded,batch_size=128,epochs=80,validation_data=(X_val,y_val_encoded))
PlotGraph(history)
RestnetModel.save('Cifar-10_RestnetModel.h5')

#Model Using inception block
input_tensor = Input(shape=(32,32,3))
convo_1 = layers.Conv2D(32,3,kernel_regularizer=regularizers.l2(0.001),activation='relu')(input_tensor)
convo_1 = layers.Dropout(0.2)(convo_1)
convo_1 = layers.BatchNormalization()(convo_1)
convo_1 = layers.MaxPooling2D(2)(convo_1)
convo2 = layers.Conv2D(64,3,kernel_regularizer=regularizers.l2(0.001),activation='relu')(convo_1)
convo2 = layers.Dropout(0.2)(convo2)
convo2 = layers.BatchNormalization()(convo2)
convo2 = layers.MaxPooling2D(2)(convo2)
#inception block
branch1 = layers.Conv2D(32,1,kernel_regularizer=regularizers.l2(0.001),activation='relu',padding='same')(convo2)
branch1 = layers.Dropout(0.2)(branch1)
branch1 = layers.BatchNormalization()(branch1)
branch2 = layers.Conv2D(32,1,kernel_regularizer=regularizers.l2(0.001),activation='relu',padding='same')(convo2)
branch2 = layers.Conv2D(32,3,kernel_regularizer=regularizers.l2(0.001),activation='relu',padding='same')(branch2)
branch2 = layers.Dropout(0.2)(branch2)
branch2 = layers.BatchNormalization()(branch2)
branch3 = layers.MaxPooling2D(3, strides=1,padding='same')(convo2)
branch3 = layers.Conv2D(32,1,activation='relu',kernel_regularizer=regularizers.l2(0.001),padding='same')(branch3)
branch3 = layers.Dropout(0.2)(branch3)
branch3 = layers.BatchNormalization()(branch3)
branch4 = layers.Conv2D(32,1,activation='relu',kernel_regularizer=regularizers.l2(0.001),padding='same')(convo2)
branch4 = layers.Conv2D(32,5,activation='relu',kernel_regularizer=regularizers.l2(0.001),padding='same')(branch4)
branch4 = layers.Dropout(0.2)(branch4)
branch4 = layers.BatchNormalization()(branch4)

output_inception = layers.concatenate([branch1,branch2,branch3,branch4],axis=-1)
output_inception = layers.Flatten()(output_inception)
output_inception = layers.Dropout(0.2)(output_inception)
output_inception = layers.BatchNormalization()(output_inception)
output_class = layers.Dense(10,activation='softmax')(output_inception)
Inception_model = Model(input_tensor,output_class)
Inception_model.summary()

Inception_model.compile(optimizer=optimizers.Adam(lr=1e-4),loss='categorical_crossentropy',metrics=['acc'])

history = Inception_model.fit(X_train,y_train_encoded,batch_size=128,epochs=80,validation_data=(X_val,y_val_encoded))
PlotGraph(history)
Inception_model.save('Cifar-10_Inception_model.h5')
#Adding dense layer for classification


def Mymodel():
    network = models.Sequential()
    network.add(layers.Conv2D(32, (3,3), activation='relu',kernel_regularizer=regularizers.l2(0.001),input_shape=(32,32,3)))
    network.add(layers.MaxPooling2D((2,2)))
    network.add(layers.Dropout(0.5))
    network.add(layers.BatchNormalization())
    network.add(layers.Conv2D(64, (3,3),kernel_regularizer=regularizers.l2(0.001),activation='relu'))
    network.add(layers.MaxPooling2D((2,2)))
    network.add(layers.Dropout(0.5))
    network.add(layers.BatchNormalization())
    network.add(layers.Flatten())
    network.add(layers.Dense(units=128, activation='relu'))
    network.add(layers.Dense(10,activation='softmax'))
    network.compile(optimizer=optimizers.Adam(lr=1e-4),loss='categorical_crossentropy',metrics=['acc'])
    return network


network = Mymodel()
network.summary()


history = network.fit(X_train,y_train_encoded,batch_size=128,epochs=80,validation_data=(X_val,y_val_encoded))
PlotGraph(history)
'''