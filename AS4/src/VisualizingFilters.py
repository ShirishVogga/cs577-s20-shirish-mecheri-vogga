# -*- coding: utf-8 -*-
"""
Created on Sat Apr 11 19:59:08 2020

@author: SHIRISH
"""

from keras.models import load_model
from keras import backend as K
import numpy as np
model = load_model('CustomDogsVsCatsModel.h5')

model.summary()

'''
for layer in model.layers:
    print(layer)
'''

layername = 'conv2d_9'
filter_idex = 0

def GenPattern(layername,filter_idex,size):
    layer_outputs = model.get_layer(layername).output
    loss = K.mean(layer_outputs[:,:,:,filter_idex])

    grad = K.gradients(loss,model.input)[0]

#Normalizing the gradients to L2 loss

    grad /= (K.sqrt(K.mean(grad))) + 1e-5

    iterate = K.function([model.input],[loss,grad])


   
#Performing gradient descent

    input_img_data  = np.random.random((1,size,size,3)) * 20 + 128

    step = 1

    for i in range(40):
        loss_value,grad_value = iterate(input_img_data)
        input_img_data += grad_value * step

    img = input_img_data[0]
    return postprocess(img)
#Post process the image
def postprocess(x):
    x -= x.mean()
    x /= (x.std()) + 1e-5
    x *= 0.1
    
    x += 0.5
    x = np.clip(x,0,1)
    x *= 255
    x = np.clip(x,0,255).astype('uint8')
    return x


import matplotlib.pyplot as plt

img = GenPattern('conv2d_12',0,150)
plt.imshow(img)

layer_names = ['conv2d_9','conv2d_10','conv2d_11']

size = 32
margin = 5
batchsize = 4

for l in layer_names:
    results = np.zeros((6*size + 7*margin,6*size + 7*margin,3))
    count =0 
    for i in range(6):
        for j in range(6):
            filter_img = GenPattern(l, i + (j*batchsize),size=size)
            horizontal_start = i * size + i * margin
            horizontal_end = horizontal_start + size
            vertical_start = j * size + j * margin
            vertical_end = vertical_start + size
            results[horizontal_start: horizontal_end,vertical_start: vertical_end, :] = filter_img
    plt.figure(figsize=(20,20))
    plt.xlabel(l)
    plt.imshow(postprocess(results))