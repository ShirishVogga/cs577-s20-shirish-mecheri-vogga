# -*- coding: utf-8 -*-
"""
Created on Wed Feb 19 08:41:20 2020

@author: SHIRISH
"""


import pandas as pd
from keras.models import load_model


def load_spam_data():
    dataset = pd.read_csv('./Data/spambase.data')
    X = dataset.iloc[:, :-1].values
    y = dataset.iloc[:, -1].values

#splitting the data set 
    from sklearn.model_selection import train_test_split
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 1/3, random_state = 0)
    return X_train,y_train,X_test,y_test

#Normalizing the data
    
X_train,y_train,X_test,y_test= load_spam_data()
X_train = X_train.astype('float32')
X_test = X_test.astype('float32')

y_train= y_train.astype('float32')
y_test = y_test.astype('float32')

mean = X_train.mean(axis=0)
X_train -= mean
std = X_train.std(axis=0)
X_train /= std


X_test -= mean
X_test /= std

saved_network = load_model("./Models/Task2Finalmodel.h5")
results = saved_network.evaluate(X_test,y_test,batch_size=128)
print(results)