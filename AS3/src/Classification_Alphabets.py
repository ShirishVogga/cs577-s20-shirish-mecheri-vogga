# -*- coding: utf-8 -*-
"""
Created on Tue Mar 17 19:58:12 2020

@author: SHIRISH
"""

import numpy as np
from keras import models
from keras import layers
import pandas as pd
from keras import regularizers
from keras.utils import to_categorical
from keras import optimizers

dataset = pd.read_csv("letter-recognition.data",header=None)
X = dataset.iloc[:,1:].values
y = dataset.iloc[:, 0].values

from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 1/3, random_state = 0)

X_train = X_train.astype('float32')
X_test = X_test.astype('float32')

from sklearn.preprocessing import LabelEncoder
le = LabelEncoder()
y_train = le.fit_transform(y_train)
y_test = le.fit_transform(y_test)

y_train= y_train.astype('float32')
y_test = y_test.astype('float32')

mean = X_train.mean(axis=0)
X_train -= mean
std = X_train.std(axis=0)
X_train /= std


X_test -= mean
X_test /= std
#one hot encoding the labels
y_train_encoded = to_categorical(y_train)
y_test_encoded = to_categorical(y_test)

def PlotGraph(history,o,l):
    import matplotlib.pyplot as plt
    acc = history.history['acc']
    val_acc = history.history['val_acc']
    loss = history.history['loss']
    val_loss = history.history['val_loss']
    epochs = range(1,len(acc) + 1)

#Validation vs Training in accuracy
    print("Graph for parameters",o,l)
    plt.plot(epochs,acc,'bo',label='training accuracy')
    plt.plot(epochs,val_acc,'b',label='Validation accuracy')
    plt.xlabel('Epochs')
    plt.ylabel('Accuracy')
    plt.legend()
    plt.figure()

    plt.plot(epochs,loss,'bo',label='training loss')
    plt.plot(epochs,val_loss,'b',label='Validation loss')
    plt.xlabel('Epochs')
    plt.ylabel('Loss')
    plt.legend()
    plt.figure()
    plt.show()


def BuildInitialModel(optimizer):
    network = models.Sequential()
    network.add(layers.Dense(units=32,activation='relu',input_shape=(X_train.shape[1], )))
    network.add(layers.Dense(units=64, activation='relu'))
    network.add(layers.Dense(units=128, activation='relu'))
    network.add(layers.Dense(units=26,activation='softmax'))
    network.compile(optimizer=optimizer,loss='categorical_crossentropy',metrics=['acc'])
    return network

#Fixing optimizer as rmsprop and evaluating the loss functions
def EvaluateLossFunctionModel(loss):
    network = models.Sequential()
    network.add(layers.Dense(units=32,activation='relu',input_shape=(X_train.shape[1], )))
    network.add(layers.Dense(units=64, activation='relu'))
    network.add(layers.Dense(units=128, activation='relu'))
    network.add(layers.Dense(units=26,activation='softmax'))
    network.compile(optimizer=optimizers.RMSprop(learning_rate=1e-4),loss=loss,metrics=['acc'])
    return network

def BuildRegularizedModelWithWeightDecay():
    network = models.Sequential()
    network.add(layers.Dense(units=32,activation='relu',kernel_regularizer=regularizers.l2(0.001),input_shape=(X_train.shape[1], )))
    network.add(layers.Dense(units=64, activation='relu'))
    network.add(layers.Dense(units=128, activation='relu',kernel_regularizer=regularizers.l2(0.001)))
    network.add(layers.Dense(units=26,activation='softmax'))
    network.compile(optimizer='Adadelta',loss='categorical_crossentropy',metrics=['acc'])
    return network

def BuildRegularizedModelWithoutDropout():
    network = models.Sequential()
    network.add(layers.BatchNormalization())
    network.add(layers.Dense(units=32,activation='relu',kernel_regularizer=regularizers.l2(0.001),input_shape=(X_train.shape[1], )))
    network.add(layers.BatchNormalization())
    network.add(layers.Dense(units=64, activation='relu'))
    network.add(layers.BatchNormalization())
    network.add(layers.Dense(units=128, activation='relu',kernel_regularizer=regularizers.l2(0.001)))
    network.add(layers.Dense(units=26,activation='softmax'))
    network.compile(optimizer='Adam',loss='categorical_crossentropy',metrics=['acc'])
    return network

#Model using weight deca,Dropout,Batch Normalization
def Regularized_FinalModel():
    network = models.Sequential()
    network.add(layers.BatchNormalization())
    network.add(layers.Dense(units=32,activation='relu',kernel_regularizer=regularizers.l2(0.001),input_shape=(X_train.shape[1], )))
    network.add(layers.BatchNormalization())
    network.add(layers.Dense(units=64, activation='relu',kernel_regularizer=regularizers.l2(0.001)))
    network.add(layers.BatchNormalization())
    network.add(layers.Dense(units=128, activation='relu',kernel_regularizer=regularizers.l2(0.001)))
    network.add(layers.Dropout(0.5))
    network.add(layers.Dense(units=26,activation='softmax'))
    network.compile(optimizer='rmsprop',loss='categorical_crossentropy',metrics=['acc'])
    return network



X_val = X_train[:2000]
X_partial_train = X_train[2000:]

y_val = y_train_encoded[:2000]
y_partial_train = y_train_encoded[2000:]
eval_loss = dict()

optimizer = ['rmsprop', 'Adagrad', 'Adadelta', 'Adam', 'Adamax', 'Nadam']
loss = ['squared_hinge','categorical_crossentropy','kullback_leibler_divergence']
for l in loss:
    network = EvaluateLossFunctionModel(l)
    history = network.fit(X_partial_train,y_partial_train,epochs=80,batch_size=128,validation_data=(X_val,y_val),verbose=0)
    PlotGraph(history,"rmsprop",l)

for o in optimizer:
    network = BuildInitialModel(o)
    history = network.fit(X_partial_train,y_partial_train,epochs=500,batch_size=128,validation_data=(X_val,y_val),verbose=0)
    PlotGraph(history,o,"categorical_cross_entropy")
    
import time as t
optimizer_dict=dict()
optimizer_acc = dict()
#to find the convergence time 
for o in optimizer:
    #Gives the time in seconds and now to perform the fit method on each optimizer and measure the difference of time
    network = BuildInitialModel(o)
    start_time = t.time() 
    history=network.fit(X_partial_train,y_partial_train,epochs=80,batch_size=128,validation_data=(X_val,y_val),verbose=0)
    #PlotGraph(history,loss_optimal,o)
    end_time = t.time()
    time_taken = end_time-start_time
    optimizer_dict[o]=time_taken
    temp = np.average(history.history['val_acc'])
    optimizer_acc[o]=temp
    print("Convergence time for optimizer ",o,"is ",time_taken)
    print("Accuracy for optimizer ",o,"is ",temp)
    

#Ensemble classifier using 3 models
network = BuildRegularizedModelWithWeightDecay()
history=network.fit(X_partial_train,y_partial_train,epochs=80,batch_size=128,validation_data=(X_val,y_val),verbose=0)
PlotGraph(history,'Adadelta','categorical_crossentropy')
results_1 = network.predict(X_test)
_,model_acc = network.evaluate(X_test,y_test_encoded)
print("Accuracy using model 1 is",model_acc)
network.save("Classfication_regularized_model1")

network = BuildRegularizedModelWithoutDropout()
history=network.fit(X_partial_train,y_partial_train,epochs=80,batch_size=128,validation_data=(X_val,y_val),verbose=0)
PlotGraph(history,'Adam','categorical_crossentropy')
results_2 = network.predict(X_test)
_,model_acc = network.evaluate(X_test,y_test_encoded)
print("Accuracy using model 2 is",model_acc)
network.save("Classfication_regularized_model2")

network = Regularized_FinalModel()
history=network.fit(X_partial_train,y_partial_train,epochs=80,batch_size=128,validation_data=(X_val,y_val),verbose=0)
PlotGraph(history,'rmsprop','categorical_crossentropy')
results_3 = network.predict(X_test)
_,model_acc = network.evaluate(X_test,y_test_encoded)
print("Accuracy using model 3 is",model_acc)
network.save("Classfication_regularized_model3")
#using ensemble classification averaging the outputs from 2 models 
avg_prediction = (results_1+results_2+results_3)/3
count = 0
for i in range(avg_prediction.shape[0]):
    if np.argmax(y_test_encoded[i]) == np.argmax(avg_prediction[i]):
        count+=1

acc = (count/avg_prediction.shape[0])*100
print("The accuracy using the ensemble classifier of model1 and model2 and model3 is ",acc)