from keras.datasets import cifar10
import numpy as np
from keras.utils import to_categorical
from keras.models import load_model
(train_data, train_label),(test_data,test_label) = cifar10.load_data()



#Since each image class comprises of 5000 images for 3 classes its 15000 immages
X_train = np.zeros((15000,32,32,3))
y_train = np.zeros((15000,1))


j=0
for i in range(train_label.shape[0]):
    if train_label[i] == 0 or train_label[i] == 1 or train_label[i] == 2:
        X_train[j] = train_data[i]
        j = j+1

#train labels
j = 0
for i in range(train_label.shape[0]):
    if train_label[i] == 0 or train_label[i] == 1 or train_label[i] == 2:
        y_train[j] = train_label[i]
        j = j+1


#Test data and labels
count = 0
for i in range(test_label.shape[0]):
    if test_label[i] == 0 or test_label[i] == 1 or test_label[i] == 2:
        count = count + 1

y_test = np.zeros((count,1))

j=0
for i in range(test_label.shape[0]):
    if test_label[i] == 0 or test_label[i] == 1 or test_label[i] == 2:
        y_test[j] = test_label[i]
        j+=1

x_test = np.zeros((count,32,32,3))
j=0
for i in range(test_label.shape[0]):
    if test_label[i] == 0 or test_label[i] == 1 or test_label[i] == 2:
        x_test[j] = test_data[i]
        j+=1
#Normalising data

X_train = X_train.reshape(15000,32*32*3)                
X_train = X_train.astype('float32')/255
y_train = y_train.astype('float32')
y_train = to_categorical(y_train)

x_test = x_test.reshape(3000,32*32*3)
x_test = x_test.astype('float32')/255

y_test = y_test.astype('float32')
y_test = to_categorical(y_test)

saved_network = load_model("./Models/Task1FinalModel.h5")
results = saved_network.evaluate(x_test,y_test,batch_size=128)
print(results)