# -*- coding: utf-8 -*-
"""
Created on Wed Apr  8 21:31:06 2020

@author: SHIRISH
"""
import numpy as np
from keras.models import load_model
from keras import models
from keras.preprocessing import image
model = load_model("CustomDogsVsCatsModel.h5")

model.summary()

#Visualizing a given image

img_name = "D:/Personal/DeepLearning 1/CatsandDogsSmall/train/dogs/dog.6.jpg"

img = image.load_img(img_name,target_size=(150,150))
img_tensor = image.img_to_array(img)

img_tensor = np.expand_dims(img_tensor, axis=0)
print(img_tensor.shape)

layers_outputs = [layer.output for layer in model.layers[:8]]

activations_model = models.Model(inputs=model.input,outputs=layers_outputs)

activations = activations_model.predict(img_tensor)
first_layer = activations[0]

import matplotlib.pyplot as plt

plt.matshow(first_layer[0,:,:,4],cmap='viridis')    


layer_names = []

for layer in model.layers[:8]:
    layer_names.append(layer)


images_per_row = 16

for layer_name,layer_activation in zip(layer_names,activations):
    print(layer_name)
    n_channels = layer_activation.shape[-1]
    size = layer_activation.shape[1]
    n_cols = n_channels // images_per_row
    
    display_grid = np.zeros((size*n_cols,images_per_row*size))
    
    for col in range(n_cols):
        for row in range(images_per_row):
            channel_image = layer_activation[0,:,:,col*images_per_row + row]
            channel_image -= channel_image.mean()
            channel_image /= channel_image.std()
            channel_image -= 64
            channel_image +=128
            channel_image = np.clip(channel_image,0,255).astype('uint8')
            display_grid[col * size : (col+1)*size,row*size : (row+1)*size] = channel_image
    
    scale = 1. / size
    plt.figure(figsize=(scale * display_grid.shape[1],scale * display_grid.shape[0]))
    plt.title(layer_name)
    plt.imshow(display_grid, aspect='auto', cmap='viridis')
    