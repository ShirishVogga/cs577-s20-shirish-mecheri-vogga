# -*- coding: utf-8 -*-
"""
Created on Mon Feb 10 19:51:49 2020

@author: SHIRISH
"""

from keras import models
from keras import layers
from keras.datasets import cifar10
import numpy as np
from keras.utils import to_categorical
from keras import regularizers
from keras import optimizers
(train_data, train_label),(test_data,test_label) = cifar10.load_data()



#Since each image class comprises of 5000 images for 3 classes its 15000 immages
X_train = np.zeros((15000,32,32,3))
y_train = np.zeros((15000,1))


j=0
for i in range(train_label.shape[0]):
    if train_label[i] == 0 or train_label[i] == 1 or train_label[i] == 2:
        X_train[j] = train_data[i]
        j = j+1

#train labels
j = 0
for i in range(train_label.shape[0]):
    if train_label[i] == 0 or train_label[i] == 1 or train_label[i] == 2:
        y_train[j] = train_label[i]
        j = j+1


#Test data and labels
count = 0
for i in range(test_label.shape[0]):
    if test_label[i] == 0 or test_label[i] == 1 or test_label[i] == 2:
        count = count + 1

y_test = np.zeros((count,1))

j=0
for i in range(test_label.shape[0]):
    if test_label[i] == 0 or test_label[i] == 1 or test_label[i] == 2:
        y_test[j] = test_label[i]
        j+=1

x_test = np.zeros((count,32,32,3))
j=0
for i in range(test_label.shape[0]):
    if test_label[i] == 0 or test_label[i] == 1 or test_label[i] == 2:
        x_test[j] = test_data[i]
        j+=1
#Normalising data

X_train = X_train.reshape(15000,32*32*3)                
X_train = X_train.astype('float32')/255
y_train = y_train.astype('float32')
y_train = to_categorical(y_train)

x_test = x_test.reshape(3000,32*32*3)
x_test = x_test.astype('float32')/255

y_test = y_test.astype('float32')
y_test = to_categorical(y_test)

#Preparing the validation data
X_val = X_train[:1000]
X_partial_train = X_train[1000:]

X_valtest = y_train[:1000]
y_partial_train = y_train[1000:] 

#Initial Network configuration
def BuildInitialModel():
    network = models.Sequential()
    network.add(layers.Dense(units=128,activation='relu',input_shape = (32 * 32 * 3, )))
    network.add(layers.Dense(units=512,activation='relu'))
    network.add(layers.Dense(units=3,activation='softmax'))
    network.compile(optimizer=optimizers.RMSprop(lr=1e-4),loss='categorical_crossentropy',metrics=['acc'])

#using optimised model with fine tuned parametrs

def BuildFinalModel():
    network = models.Sequential()
    network.add(layers.Dense(units=32,activation='relu',kernel_regularizer=regularizers.l2(0.001),input_shape = (32 * 32 * 3, )))
    network.add(layers.Dense(units=64,activation='relu',kernel_regularizer=regularizers.l2(0.001)))
    network.add(layers.Dropout(0.5))
    network.add(layers.Dense(units=3,activation='softmax'))
    network.compile(optimizer=optimizers.RMSprop(lr=1e-4),loss='categorical_crossentropy',metrics=['acc'])

def PlotGraph(history):
    import matplotlib.pyplot as plt



    acc = history.history['acc']
    val_acc = history.history['val_acc']
    loss = history.history['loss']
    val_loss = history.history['val_loss']
    
    epochs = range(1,len(acc) + 1)

#Validation vs Training in accuracy
    plt.plot(epochs,acc,'bo',label='training accuracy')
    plt.plot(epochs,val_acc,'b',label='Validation accuracy')
    plt.xlabel('Epochs')
    plt.ylabel('Accuracy')
    plt.legend()
    plt.figure()
    
    plt.plot(epochs,loss,'bo',label='training loss')
    plt.plot(epochs,val_loss,'b',label='Validation loss')
    plt.xlabel('Epochs')
    plt.ylabel('Loss')
    plt.legend()
    plt.figure()


    plt.show()

network = BuildInitialModel()
history = network.fit(X_partial_train,y_partial_train,epochs=80,batch_size=128,validation_data=(X_val,X_valtest))
PlotGraph(history)
network.save("./Models/Task1initialModel.h5")
network = BuildFinalModel()
history = network.fit(X_partial_train,y_partial_train,epochs=80,batch_size=128,validation_data=(X_val,X_valtest))
PlotGraph(history)
network.save("./Models/Task1FinalModel.h5")

