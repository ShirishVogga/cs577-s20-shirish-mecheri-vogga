# -*- coding: utf-8 -*-
"""
Created on Tue Feb 11 10:34:47 2020

@author: SHIRISH
"""

from keras import models
from keras import layers
import pandas as pd
from keras import regularizers
from keras import optimizers
from keras.models import load_model


def load_spam_data():
    dataset = pd.read_csv('./Data/spambase.data')
    X = dataset.iloc[:, :-1].values
    y = dataset.iloc[:, -1].values

#splitting the data set 
    from sklearn.model_selection import train_test_split
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 1/3, random_state = 0)
    return X_train,y_train,X_test,y_test

#Normalizing the data
    
X_train,y_train,X_test,y_test= load_spam_data()
X_train = X_train.astype('float32')
X_test = X_test.astype('float32')

y_train= y_train.astype('float32')
y_test = y_test.astype('float32')

mean = X_train.mean(axis=0)
X_train -= mean
std = X_train.std(axis=0)
X_train /= std


X_test -= mean
X_test /= std


#Intial model Configuration
def BuildInitialModel():
    network = models.Sequential()
    network.add(layers.Dense(units=128,activation='relu',input_shape=(X_train.shape[1], )))
    network.add(layers.Dense(units=512, activation='relu'))
    network.add(layers.Dense(units=1,activation='sigmoid'))
    network.compile(optimizer=optimizers.RMSprop(learning_rate=1e-4),loss='binary_crossentropy',metrics=['acc'])
    return network
#Final Model Configuration to avoid overfitting problem

#Network Configuration Final model
def BuildFinalModel():
    network = models.Sequential()
    network.add(layers.Dense(units=64,activation='relu',kernel_regularizer=regularizers.l2(0.001),input_shape=(X_train.shape[1], )))
    network.add(layers.Dense(units=128, activation='relu',kernel_regularizer=regularizers.l2(0.001)))
    network.add(layers.Dropout(0.5))
    network.add(layers.Dense(units=1,activation='sigmoid'))
    network.compile(optimizer=optimizers.RMSprop(learning_rate=1e-4),loss='binary_crossentropy',metrics=['acc'])
    return network
#Plotting graph
def PlotGraph(history):
    import matplotlib.pyplot as plt
    acc = history.history['acc']
    val_acc = history.history['val_acc']
    loss = history.history['loss']
    val_loss = history.history['val_loss']
    epochs = range(1,len(acc) + 1)

#Validation vs Training in accuracy
    plt.plot(epochs,acc,'bo',label='training accuracy')
    plt.plot(epochs,val_acc,'b',label='Validation accuracy')
    plt.xlabel('Epochs')
    plt.ylabel('Accuracy')
    plt.legend()
    plt.figure()

    plt.plot(epochs,loss,'bo',label='training loss')
    plt.plot(epochs,val_loss,'b',label='Validation loss')
    plt.xlabel('Epochs')
    plt.ylabel('Loss')
    plt.legend()
    plt.figure()
    plt.show()

#Desiging validation set

X_val = X_train[:1000]
X_partial_train = X_train[1000:]

X_valtest = y_train[:1000]
y_partial_train = y_train[1000:]


network = BuildInitialModel()
history = network.fit(X_partial_train,y_partial_train,epochs=80,batch_size=128,validation_data=(X_val,X_valtest))
PlotGraph(history)
network.save("./Models/Task2Initialmodel.h5")
network = BuildFinalModel()
history = network.fit(X_partial_train,y_partial_train,epochs=80,batch_size=128,validation_data=(X_val,X_valtest))
PlotGraph(history)
network.save("./Models/Task2Finalmodel.h5")
