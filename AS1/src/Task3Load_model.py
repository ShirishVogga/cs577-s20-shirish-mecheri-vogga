# -*- coding: utf-8 -*-
"""
Created on Wed Feb 19 08:43:41 2020

@author: SHIRISH
"""

import pandas as pd
import numpy as np
from keras.models import load_model


#Using option header = none so it avoids using first row as coulumn name 


def load_data():
    dataset = pd.read_csv('./Data/communities.data',header=None)
    X = dataset.iloc[:, 5:-1].values
    y = dataset.iloc[:, -1].values
    #Since data has missing values hence first replacing ? with np.nan and den passing to the imputer class
    for i in range(X.shape[0]):
        for j in range(X.shape[1]):
            if X[i][j] == '?':
                X[i][j] = np.nan
                                
    from sklearn.impute import SimpleImputer
    imp_x = SimpleImputer(missing_values=np.nan,strategy='mean')
    imp_x = imp_x.fit(X)
    X = imp_x.fit_transform(X)
    from sklearn.model_selection import train_test_split
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.25, random_state = 0)
    return X_train,y_train,X_test,y_test



X_train,y_train,X_test,y_test = load_data()


#Preprocessing the data
X_train = np.array(X_train).astype('float32')
X_test = np.array(X_test).astype('float32')
y_train = np.array(y_train).astype('float32')
y_test = np.array(y_test).astype('float32')

#Load up trained model and test it
saved_network = load_model("./Models/Task3Finalmodel.h5")
test_mse_score,test_mae_score = saved_network.evaluate(X_test,y_test)