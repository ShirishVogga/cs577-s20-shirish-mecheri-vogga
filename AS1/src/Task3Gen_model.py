# -*- coding: utf-8 -*-
"""
Created on Tue Feb 11 14:57:39 2020

@author: SHIRISH
"""

import pandas as pd
from keras import layers
from keras import models
from keras import regularizers
from keras import optimizers
import numpy as np
from keras.models import load_model


#Using option header = none so it avoids using first row as coulumn name 


def load_data():
    dataset = pd.read_csv('./Data/communities.data',header=None)
    X = dataset.iloc[:, 5:-1].values
    y = dataset.iloc[:, -1].values
    #Since data has missing values hence first replacing ? with np.nan and den passing to the imputer class
    for i in range(X.shape[0]):
        for j in range(X.shape[1]):
            if X[i][j] == '?':
                X[i][j] = np.nan
                                
    from sklearn.impute import SimpleImputer
    imp_x = SimpleImputer(missing_values=np.nan,strategy='mean')
    imp_x = imp_x.fit(X)
    X = imp_x.fit_transform(X)
    from sklearn.model_selection import train_test_split
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.25, random_state = 0)
    return X_train,y_train,X_test,y_test



X_train,y_train,X_test,y_test = load_data()


#Preprocessing the data
X_train = np.array(X_train).astype('float32')
X_test = np.array(X_test).astype('float32')
y_train = np.array(y_train).astype('float32')
y_test = np.array(y_test).astype('float32')

def buildIntialModel():
    network = models.Sequential()
    network.add(layers.Dense(units=128,activation='relu',input_shape=(X_train.shape[1], )))
    network.add(layers.Dense(units=512, activation='relu'))
    network.add(layers.Dense(1))
    
    network.compile(optimizer='SGD',loss='mse',metrics=['mae'])
    return network

def buildFinalModel():
    network = models.Sequential()
    network.add(layers.Dense(units=64,activation='relu',input_shape=(X_train.shape[1], )))
    network.add(layers.Dense(units=64, activation='relu'))
    network.add(layers.Dense(1))
    
    network.compile(optimizer='SGD',loss='mse',metrics=['mae'])
    return network
#To perform K-Fold validation

def KfoldValidation(network):
    k = 5
    num_samples = len(X_train) // k
    num_epochs = 100
    allscore_history = []

    for i in range(k):
        print("Processing Fold #", i)
        val_data = X_train[i * num_samples:(i+1) * num_samples]
        val_test = y_train[i * num_samples:(i+1) * num_samples]
        partial_train_data = np.concatenate([X_train[:i * num_samples], X_train[(i+1) * num_samples:]],axis=0)
        partial_train_label = np.concatenate([y_train[:i * num_samples], y_train[(i+1) * num_samples:]],axis=0)
        history = network.fit(partial_train_data,partial_train_label,epochs=num_epochs,batch_size=1,verbose=0,validation_data=(val_data,val_test))
        mae_history = history.history['val_mae']
        allscore_history.append(mae_history)
    return allscore_history,num_epochs


def smoothing(points,factor=0.9):
    smoothed_pts = []
    for p in points:
        if smoothed_pts:
            previous = smoothed_pts[-1]
            smoothed_pts.append(previous * factor + p * (1-factor))
        else:
            smoothed_pts.append(p)
    return smoothed_pts

def PlotGraph(allscore_history,num_epochs):
    
    avg_mae_history = [np.mean([x[i] for x in allscore_history]) for i in range(num_epochs)]

    import matplotlib.pyplot as plt


    plt.plot(range(1,len(avg_mae_history) + 1),avg_mae_history)
    plt.xlabel('Epochs')
    plt.ylabel('Validation MAE')
    plt.show()
    
#since first 10 data points are not proper omit them

    smooth_mae_history = smoothing(avg_mae_history[10:])

    plt.plot(range(1,len(smooth_mae_history) + 1),smooth_mae_history)
    plt.xlabel('Epochs')
    plt.ylabel('Validation MAE')
    plt.show()

#initial model
network = buildIntialModel()
allscore_history,num_epochs = KfoldValidation(network)
PlotGraph(allscore_history,num_epochs)
network.fit(X_train,y_train,epochs=80,batch_size=16)
network.save("./Models/Task3Initialmodel.h5")

network = buildFinalModel()
allscore_history,num_epochs = KfoldValidation(network)
PlotGraph(allscore_history,num_epochs)
network.fit(X_train,y_train,epochs=80,batch_size=16)
network.save("./Models/Task3FinalModel.h5")
