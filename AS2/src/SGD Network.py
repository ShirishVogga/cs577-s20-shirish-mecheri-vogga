# -*- coding: utf-8 -*-
"""
Created on Tue Mar  3 10:29:45 2020

@author: SHIRISH
"""

import pandas as pd
import numpy as np
from sklearn.datasets import load_wine
from sklearn.datasets import load_iris
import matplotlib.pyplot as plt
import pickle
def loadWindesDataset():
    from sklearn.model_selection import train_test_split
    dig = load_wine()
    onehot_target = pd.get_dummies(dig.target)
    x_train, x_val, y_train, y_val = train_test_split(dig.data, onehot_target, test_size=0.2)
    return x_train, x_val, y_train, y_val

def loadIrisDataset():
    from sklearn.model_selection import train_test_split
    dig = load_iris()
    onehot_target = pd.get_dummies(dig.target)
    x_train, x_val, y_train, y_val = train_test_split(dig.data, onehot_target, test_size=0.2)
    return x_train, x_val, y_train, y_val

x_train, x_val, y_train, y_val = loadWindesDataset() #replace the function name to test on each dataset

x_train = x_train.astype('float32')
x_val = x_val.astype('float32')
mean = x_train.mean(axis=0)
x_train -= mean
std = x_train.std(axis=0)
x_train /= std
    
x_val -= mean
x_val /= std

y_train=np.array(y_train)


def plot():
    epochs = range(1,len(model.loss_mean) + 1)
    loss = model.loss_mean
    plt.plot(epochs,loss,'bo',label='training loss')
    plt.xlabel('Epochs')
    plt.ylabel('loss')
    plt.legend()
    plt.figure()

def sigmoid(s):
    return 1/(1 + np.exp(-s))

def sigmoid_derv(s):
    return s * (1 - s)

def softmax(s):
    exps = np.exp(s - np.max(s, axis=1, keepdims=True))
    return exps/np.sum(exps, axis=1, keepdims=True)

def cross_entropy(pred, real):
    n_samples = real.shape[0]
    res = pred - real
    return res/n_samples

def SGDcross_entropy(X,y):
    l = -np.log(X[0,y])
    loss = np.sum(l)
    return loss




class Dlnet:
    def __init__(self, x, y,units):
        self.x = x
        neurons = units
        self.lr = 0.003
        ip_dim = x.shape[1]
        op_dim = 3
        self.loss={}
        self.loss_mean=[]
        self.decay = 0.001/50
        self.w1 = np.random.randn(ip_dim, neurons)
        self.b1 = np.zeros((1, neurons))
        self.w2 = np.random.randn(neurons, neurons)
        self.b2 = np.zeros((1, neurons))
        self.w3 = np.random.randn(neurons, 3)
        self.b3 = np.zeros((1, 3))
        self.y = y

    def feedforward(self,x):
        z1 = np.dot(x, self.w1) + self.b1
        self.a1 = sigmoid(z1)
        z2 = np.dot(self.a1, self.w2) + self.b2
        self.a2 = sigmoid(z2)
        z3 = np.dot(self.a2, self.w3) + self.b3
        self.a3 = softmax(z3) #
    def backprop(self,x,y):
        loss = SGDcross_entropy(self.a3,y)
        dloss_yh = cross_entropy(self.a3, y) # w3
        dloss_z2 = np.dot(dloss_yh, self.w3.T)
        dloss_a2 = dloss_z2 * sigmoid_derv(self.a2) # w2
        dloss_z1 = np.dot(dloss_a2, self.w2.T)
        dloss_a1 = dloss_z1 * sigmoid_derv(self.a1) # w1
        
        self.lr*= (1.0 / (1.0 + (self.decay * 1)))
        
        self.w3 -= self.lr * np.dot(self.a2.T, dloss_yh)
        self.b3 -= self.lr * np.sum(dloss_yh, axis=0, keepdims=True)
        self.w2 -= self.lr * np.dot(self.a1.T, dloss_a2)
        self.b2 -= self.lr * np.sum(dloss_a2, axis=0)
        self.w1 -= self.lr * np.dot(x.T, dloss_a1)
        self.b1 -= self.lr * np.sum(dloss_a1, axis=0)
        return loss
    def predict(self, data):
        self.x = data
        self.feedforward(data)
        return self.a3.argmax()

def get_acc(x, y):
    acc = 0
    for xx,yy in zip(x, y): #to iterate through the feature and label vectors
        s = model.predict(xx)
        if s == np.argmax(yy):
            acc +=1
    return acc/len(x)*100

def SaveWeights_Wines():
    pickle.dump([model.w1,model.w2,model.w3,model.b1,model.b2,model.b3], open("SavedWeightsForWine.p", "wb"))

def LoadWeights_Wines():
   model.w1,model.w2,model.w3,model.b1,model.b2,model.b3=pickle.load(open("SavedWeightsForWine.p","rb"))
   print("Training accuracy : ", get_acc(x_train, np.array(y_train)))
   print("Test accuracy : ", get_acc(x_val, np.array(y_val)))

def SaveWeights_Iris():
    pickle.dump([model.w1,model.w2,model.w3,model.b1,model.b2,model.b3], open("SavedWeightsForIris.p", "wb"))

def LoadWeights_Iris():
   model.w1,model.w2,model.w3,model.b1,model.b2,model.b3=pickle.load(open("SavedWeightsForIris.p","rb"))
   print("Training accuracy : ", get_acc(x_train, np.array(y_train)))
   print("Test accuracy : ", get_acc(x_val, np.array(y_val)))

#Impact of using different units in the model 			
model = Dlnet(x_train, np.array(y_train),8)


#Testing models for different units and saving the best one
units = [8,16,32,64]
epochs = 50
for i in units:
    model = Dlnet(x_train, np.array(y_train),i)
    for x in range(epochs):
        for i in range(x_train.shape[0]):
            model.feedforward(x_train[i].reshape(1,x_train.shape[1]))
            loss=model.backprop(x_train[i].reshape(1,x_train.shape[1]),y_train[i])
            model.loss[i]=loss
        model.loss_mean.append(np.mean(list(model.loss.values())))
    plot()
    flag=0	
    print("Training accuracy : ", get_acc(x_train, np.array(y_train)))
    print("Test accuracy : ", get_acc(x_val, np.array(y_val)))
    
    
    '''
    if get_acc(x_train, np.array(y_train)) and get_acc(x_val, np.array(y_val)) > 80.0:
        SaveWeights_Wines()
        flag=1
    if flag == 1:
        LoadWeights_Wines()
    
    #To test whether load weights work
#pickle.dump([model.loss_mean], open("LossValuesForIris.p","wb"))
'''