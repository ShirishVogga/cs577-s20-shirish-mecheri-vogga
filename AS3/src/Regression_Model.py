
# Use scikit-learn to grid search the batch size and epochs
import numpy as np
from keras import models
from keras import layers
import pandas as pd
from keras import regularizers
from keras.utils import to_categorical
from keras import optimizers
# Function to create model, required for KerasClassifier
    def create_model(loss):
    	# create model
        network = models.Sequential()
        network.add(layers.Dense(units=32,activation='relu',input_shape=(X_train.shape[1], )))
        network.add(layers.Dense(units=64, activation='relu'))
        network.add(layers.Dense(units=1))	
    	# Compile model
        network.compile(optimizer='Adam',loss=loss,metrics=['mae'])
        return network

def RegularizedModel1():
	# create model
    network = models.Sequential()
    network.add(layers.Dense(units=32,activation='relu',kernel_regularizer=regularizers.l2(0.001),input_shape=(X_train.shape[1], )))
    network.add(layers.Dense(units=64,kernel_regularizer=regularizers.l2(0.001),activation='relu'))
    network.add(layers.Dense(units=1))	
	# Compile model
    network.compile(optimizer='Adamax',loss=key,metrics=['mae'])
    return network

def RegularizedModel2():
	# create model
    network = models.Sequential()
    network.add(layers.BatchNormalization())
    network.add(layers.Dense(units=32,activation='relu',kernel_regularizer=regularizers.l2(0.001),input_shape=(X_train.shape[1], )))
    network.add(layers.BatchNormalization())
    network.add(layers.Dense(units=64,kernel_regularizer=regularizers.l2(0.001),activation='relu'))
    network.add(layers.BatchNormalization())
    network.add(layers.Dense(units=64,kernel_regularizer=regularizers.l2(0.001),activation='relu'))
    network.add(layers.Dense(units=1))	
	# Compile model
    network.compile(optimizer='Nadam',loss=key,metrics=['mae'])
    return network

def RegularizedModel3():
	# create model
    network = models.Sequential()
    network.add(layers.BatchNormalization())
    network.add(layers.Dense(units=32,activation='relu',kernel_regularizer=regularizers.l2(0.001),input_shape=(X_train.shape[1], )))
    network.add(layers.BatchNormalization())
    network.add(layers.Dense(units=64,kernel_regularizer=regularizers.l2(0.001),activation='relu'))
    network.add(layers.BatchNormalization())
    network.add(layers.Dense(units=64,kernel_regularizer=regularizers.l2(0.001),activation='relu'))
    network.add(layers.Dropout(0.5))
    network.add(layers.Dense(units=1))	
	# Compile model
    network.compile(optimizer='Adagrad',loss=key,metrics=['mae'])
    return network

def OptimialOptimizer(o):
    network = models.Sequential()
    network.add(layers.Dense(units=32,activation='relu',input_shape=(X_train.shape[1], )))
    network.add(layers.Dense(units=64, activation='relu'))
    network.add(layers.Dense(units=1))	
	# Compile model
    network.compile(optimizer=o,loss=key,metrics=['mae'])
    return network
    

def smoothing(points,factor=0.9):
    smoothed_pts = []
    for p in points:
        if smoothed_pts:
            previous = smoothed_pts[-1]
            smoothed_pts.append(previous * factor + p * (1-factor))
        else:
            smoothed_pts.append(p)
    return smoothed_pts

def PlotGraph(history,optimizer,loss):
    import matplotlib.pyplot as plt
    val_mae = history.history['val_mae']
    print("Graph for parameters",optimizer,loss)
    smooth_mae_history = smoothing(val_mae[10:])

    plt.plot(range(1,len(smooth_mae_history) + 1),smooth_mae_history)
    plt.xlabel('Epochs')
    plt.ylabel('Validation MAE')
    plt.show()


def mae(actual, predicted):
    sum_error = 0.0
    for i in range(len(actual)):
        sum_error += abs(predicted[i] - actual[i])
    return sum_error / float(len(actual))

# load dataset
dataset = pd.read_csv("abalone.data",header=None)
# split into input (X) and output (Y) variables
#ignoring the gender column 
X = dataset.iloc[:,1:-1]
Y = dataset.iloc[:,-1]

#Splitting them into train,test and validate

from sklearn.model_selection import train_test_split
X_train,X_test,y_train,y_test = train_test_split(X,Y,test_size=0.25,random_state = 1)

#one hot encoding the columns 

#Converting the dataset to float32
X_train = np.array(X_train).astype('float32')
y_train = np.array(y_train).astype('float32')
X_test = np.array(X_test).astype('float32')
y_test = np.array(y_test).astype('float32')

#Normalizing dataset
mean = X_train.mean(axis=0)
X_train -= mean
std = X_train.std(axis=0)
X_train /= std
    
X_test -= mean
X_test /= std


#Creating valiidation
X_val = X_train[:1000]
X_partial_train = X_train[1000:]
y_val = y_train[:1000]
y_partial_train = y_train[1000:]
eval_dic = dict() 
# define the grid search parameters

optimizer = ['rmsprop', 'Adagrad', 'Adadelta', 'Adam', 'Adamax', 'Nadam']
loss = ['huber_loss','mse','logcosh']

for l in loss:
    network = create_model(l)
    history = network.fit(X_partial_train,y_partial_train,epochs=80,batch_size=128,validation_data=(X_val,y_val),verbose=0)
    temp = np.average(history.history['val_mae'])
    eval_dic[l] = temp
    #PlotGraph(history,"Adam",l)
        

#deciding on loss function
small = 999
for i in eval_dic.values():
    if i < small:
        small = i

for key,value in eval_dic.items():
    if value == small:
        loss_optimal = key
        print("The loss function is suitable is ",key)


#To select the required optimizer
optimizer_dict = {}
optimizer_perf = {}
for o in optimizer:
    network = OptimialOptimizer(o) 
    history=network.fit(X_partial_train,y_partial_train,epochs=500,batch_size=128,validation_data=(X_val,y_val),verbose=0)
    PlotGraph(history,o,key)
    

#From looking at the graphs we find the epoch number to be 200 for every optimizer to converge
#to calculate the convergence time for each model
    
import time as t
for o in optimizer:
    network = OptimialOptimizer(o)
    start_time = t.time()
    history=network.fit(X_partial_train,y_partial_train,epochs=200,batch_size=128,validation_data=(X_val,y_val),verbose=0)
    end_time = t.time()
    time_taken = end_time-start_time
    optimizer_dict[o]=time_taken
    temp = np.average(history.history['val_mae'])
    optimizer_perf[o]=temp
    print("time taken for each optimizer",o," is ",time_taken)
    #PlotGraph(history,o,key)
    
    
#From the above convergence time we find that Adagrad has the least convergence time and also the least Mean Absolute error

#Regularization tests on each model and performing ensemble regression
network = RegularizedModel1()
history=network.fit(X_partial_train,y_partial_train,epochs=200,batch_size=128,validation_data=(X_val,y_val),verbose=0)
PlotGraph(history,"Adamax",key)
_,test_mae = network.evaluate(X_test,y_test)
result1 = network.predict(X_test)
print("The test set mae for model 1 is ",test_mae)
network.save("Regression_model1Regularized")

network = RegularizedModel2()
history=network.fit(X_partial_train,y_partial_train,epochs=200,batch_size=128,validation_data=(X_val,y_val),verbose=0)
PlotGraph(history,"Nadam",key)
_,test_mae = network.evaluate(X_test,y_test)
result2 = network.predict(X_test)
print("The test set mae for model 2 is ",test_mae)
network.save("Regression_model2Regularized")

network = RegularizedModel3()
history=network.fit(X_partial_train,y_partial_train,epochs=200,batch_size=128,validation_data=(X_val,y_val),verbose=0)
PlotGraph(history,"Adagrad",key)
_,test_mae = network.evaluate(X_test,y_test)
result3 = network.predict(X_test)
print("The test set mae for model 3 is ",test_mae)
network.save("Regression_model3Regularized")

average_result = (result1+result2+result3)/3

Ensemble_result = mae(y_test,average_result)
print("The result of the ensembled regression model is =",Ensemble_result)