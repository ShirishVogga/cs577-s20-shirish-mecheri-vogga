# -*- coding: utf-8 -*-
"""
Created on Fri Feb  7 12:26:37 2020

@author: SHIRISH
"""
import os, shutil

#Creating filenames
original_dataset_dir = 'D:\Personal\DeepLearning 1\KaggleDataset\Train'

base_dir = 'D:\Personal\DeepLearning 1\CatsandDogsSmall'
train_dir = os.path.join(base_dir,'train')
validation_dir = os.path.join(base_dir,'validation')
train_cats_dir = os.path.join(train_dir,'cats')
train_dogs_dir = os.path.join(train_dir,'dogs')
test_dir = os.path.join(base_dir,'test')
test_cats_dir = os.path.join(test_dir,'cats')
test_dogs_dir = os.path.join(test_dir,'dogs')
validation_cats_dir = os.path.join(validation_dir,'cats')
validation_dogs_dir = os.path.join(validation_dir,'dogs')
if os.path.exists(base_dir)==False:
    os.mkdir(base_dir)
    os.mkdir(train_dir)
    os.mkdir(test_dir)
    os.mkdir(validation_dir)
    os.mkdir(train_cats_dir)
    os.mkdir(train_dogs_dir)
    os.mkdir(validation_cats_dir)
    os.mkdir(validation_dogs_dir)
    os.mkdir(test_cats_dir)
    os.mkdir(test_dogs_dir)

fname = ['cat.{}.jpg'.format(i) for i in range(1000)]          #Training Set cat
for f in fname:                                            
    src = os.path.join(original_dataset_dir,f)
    dst = os.path.join(train_cats_dir,f)
    if os.path.exists(dst)==False:
        shutil.copyfile(src,dst)
    
fnames = ['cat.{}.jpg'.format(i) for i in range(1000,1500)]          #validation set cat
for f in fnames:
    src = os.path.join(original_dataset_dir,f)
    dst = os.path.join(validation_cats_dir,f)
    if os.path.exists(dst)==False:
        shutil.copyfile(src,dst)

fnames = ['cat.{}.jpg'.format(i) for i in range(1500,2000)]         #Test Set cat
for f in fnames:
    src = os.path.join(original_dataset_dir,f)
    dst = os.path.join(test_cats_dir,f)
    if os.path.exists(dst)==False:
        shutil.copyfile(src,dst)
    
fnames = ['dog.{}.jpg'.format(i) for i in range(1000)]
for f in fnames:
    src = os.path.join(original_dataset_dir,f)
    dst = os.path.join(train_dogs_dir,f)
    if os.path.exists(dst)==False:
        shutil.copyfile(src,dst)
    
fnames = ['dog.{}.jpg'.format(i) for i in range(1000,1500)]
for f in fnames:
    src = os.path.join(original_dataset_dir,f)
    dst = os.path.join(validation_dogs_dir,f)
    if os.path.exists(dst)==False:
        shutil.copyfile(src,dst)

fnames = ['dog.{}.jpg'.format(i) for i in range(1500,2000)]
for f in fnames:
    src = os.path.join(original_dataset_dir,f)
    dst = os.path.join(test_dogs_dir,f)
    if os.path.exists(dst)==False:
        shutil.copyfile(src,dst)
    
#Building the model
from keras import models
from keras import layers
from keras import optimizers
from keras import regularizers
from keras.preprocessing import image
from keras.applications import VGG16
from keras.preprocessing.image import ImageDataGenerator


def PretrainedNetworkWithFrozenWeights():
    conv_base = VGG16(weights = 'imagenet',
                  include_top = False,
                  input_shape=(150,150,3))

    conv_base.summary()
    return conv_base

def getFinalModel():
    network = models.Sequential()
    network.add(layers.Conv2D(32, (3,3), activation='relu',kernel_regularizer=regularizers.l2(0.001),input_shape=(150, 150, 3)))
    network.add(layers.MaxPooling2D((2,2)))
    network.add(layers.Conv2D(64, (3,3),kernel_regularizer=regularizers.l2(0.001),activation='relu'))
    network.add(layers.MaxPooling2D((2,2)))
    network.add(layers.Conv2D(128, (3,3),kernel_regularizer=regularizers.l2(0.001),activation='relu'))
    network.add(layers.MaxPooling2D((2,2)))
    network.add(layers.Conv2D(128, (3,3),kernel_regularizer=regularizers.l2(0.001),activation='relu'))
    network.add(layers.MaxPooling2D((2,2)))
    network.add(layers.BatchNormalization())
    network.add(layers.Flatten())
    network.add(layers.Dropout(0.5))
    network.add(layers.Dense(units=512, activation='relu'))
    network.add(layers.Dense(1,activation='sigmoid'))
    network.compile(optimizer=optimizers.Adam(lr=1e-4),loss='binary_crossentropy',metrics=['acc'])
    return network

def getInitialModel():
    network = models.Sequential()
    network.add(layers.Conv2D(32, (5,5), activation='relu',kernel_regularizer=regularizers.l2(0.001),input_shape=(150, 150, 3)))
    network.add(layers.MaxPooling2D((2,2)))
    network.add(layers.Conv2D(64, (5,5),kernel_regularizer=regularizers.l2(0.001),activation='relu'))
    network.add(layers.MaxPooling2D((2,2)))
    network.add(layers.Conv2D(128, (5,5),kernel_regularizer=regularizers.l2(0.001),activation='relu'))
    network.add(layers.MaxPooling2D((2,2)))
    network.add(layers.Conv2D(256, (5,5),kernel_regularizer=regularizers.l2(0.001),activation='relu'))
    network.add(layers.MaxPooling2D((2,2)))
    network.add(layers.BatchNormalization())
    network.add(layers.Flatten())
    network.add(layers.Dropout(0.5))
    network.add(layers.Dense(units=512, activation='relu'))
    network.add(layers.Dense(1,activation='sigmoid'))
    network.compile(optimizer=optimizers.Adam(lr=1e-4),loss='binary_crossentropy',metrics=['acc'])
    return network
    
def PlotGraph(history):
    import matplotlib.pyplot as plt
    acc = history.history['acc']
    val_acc = history.history['val_acc']
    loss = history.history['loss']
    val_loss = history.history['val_loss']
    epochs = range(1,len(acc) + 1)

#Validation vs Training in accuracy
    plt.plot(epochs,acc,'bo',label='training accuracy')
    plt.plot(epochs,val_acc,'b',label='Validation accuracy')
    plt.xlabel('Epochs')
    plt.ylabel('Accuracy')
    plt.legend()
    plt.figure()

    plt.plot(epochs,loss,'bo',label='training loss')
    plt.plot(epochs,val_loss,'b',label='Validation loss')
    plt.xlabel('Epochs')
    plt.ylabel('Loss')
    plt.legend()
    plt.figure()
    plt.show()
#Getting images from dataset


def GetAgumentedDatagen():
    test_datagen = ImageDataGenerator(rescale=1./255)
    train_datagen = ImageDataGenerator(
            rescale=1./255,
            rotation_range=40,
            width_shift_range=0.2,
            height_shift_range=0.2,
            shear_range=0.2,
            zoom_range=0.2,
            horizontal_flip=True,
            fill_mode='nearest')
    return train_datagen,test_datagen

def GetDatagen():
    test_datagen = ImageDataGenerator(rescale=1./255)
    train_datagen = ImageDataGenerator(
                    rescale=1./255,
                    fill_mode='nearest')
    return train_datagen,test_datagen

#Using agumented datagen


def Generator_with_AugmentedDatagen():
    train_datagen,test_datagen = GetAgumentedDatagen()  
    train_generator = train_datagen.flow_from_directory(
        train_dir,
        target_size=(150,150),
        batch_size=20,
        class_mode='binary')

    validation_generator = test_datagen.flow_from_directory(
        validation_dir,
        target_size=(150,150),
        batch_size=20,
        class_mode='binary')
    return train_generator,validation_generator
    

def Generator_without_Augmentation():
    train_datagen,test_datagen = GetDatagen()  
    train_generator = train_datagen.flow_from_directory(
        train_dir,
        target_size=(150,150),
        batch_size=20,
        class_mode='binary')

    validation_generator = test_datagen.flow_from_directory(
        validation_dir,
        target_size=(150,150),
        batch_size=20,
        class_mode='binary')
    return train_generator,validation_generator
    


#Training initial model without agumentation
train_generator,validiation_generator = Generator_without_Augmentation()
network = getInitialModel()
network.summary()
history = network.fit_generator(train_generator,steps_per_epoch=100,epochs=80,validation_data=validiation_generator,validation_steps=100)
PlotGraph(history)


#Training final model with hyperparameter optimization such reducing receptive field   
train_generator,validiation_generator=Generator_without_Augmentation()
network = getFinalModel()
history = network.fit_generator(train_generator,
                      steps_per_epoch=100,
                      epochs=80,
                      validation_data=validiation_generator,
                      validation_steps=100)

PlotGraph(history)


conv_base = PretrainedNetworkWithFrozenWeights()

#using pretrained network with  frozen weights
def VGG16FrozenNetwork():
    network = models.Sequential()
    network.add(conv_base)
    network.add(layers.Flatten())
    network.add(layers.Dropout(0.5))
    network.add(layers.Dense(units=512, activation='relu'))
    network.add(layers.Dense(1,activation='sigmoid'))
    network.summary()
    #freeze the wieghts of convolution base so as to not to change the weights of the already learned network
    conv_base.trainable = False
    network.summary()
    network.compile(optimizer=optimizers.Adam(lr=1e-4),loss='binary_crossentropy',metrics=['acc'])
    return network


def VGG16finetuned():
    #Training again by unfreezing some layers

    conv_base.trainable = True
    set_trainable = False

    for layer in conv_base.layers:
        if layer.name == 'block5_conv1':
            set_trainable=True
        if set_trainable:
            layer.trainable=True
        else:
            layer.trainable=False

    conv_base.summary()  
    network = models.Sequential()
    network.add(conv_base)
    network.add(layers.Flatten())
    network.add(layers.Dropout(0.5))
    network.add(layers.Dense(units=512, activation='relu'))
    network.add(layers.Dense(1,activation='sigmoid'))
    network.summary()
    network.compile(optimizer=optimizers.Adam(lr=1e-4),loss='binary_crossentropy',metrics=['acc'])
    return network



PlotGraph(history)

#Frozen Vgg16 without data agumentation
network = VGG16FrozenNetwork()
train_datagen,test_datagen = GetDatagen()  
train_generator = train_datagen.flow_from_directory(
        train_dir,
        target_size=(150,150),
        batch_size=20,
        class_mode='binary')

validation_generator = test_datagen.flow_from_directory(
        validation_dir,
        target_size=(150,150),
        batch_size=20,
        class_mode='binary')

history = network.fit_generator(train_generator,
                      steps_per_epoch=100,
                      epochs=80,
                      validation_data=validation_generator,
                      validation_steps=100)

network.save('Cats&Dogs_VGG16FrozenWOAugmentation.h5')
PlotGraph(history)

#Fine tuned VGG16 with layers unfrozen with agumented datagen
network = VGG16finetuned()
train_datagen,test_datagen = GetAgumentedDatagen()  
train_generator = train_datagen.flow_from_directory(
        train_dir,
        target_size=(150,150),
        batch_size=20,
        class_mode='binary')

validation_generator = test_datagen.flow_from_directory(
        validation_dir,
        target_size=(150,150),
        batch_size=20,
        class_mode='binary')

history = network.fit_generator(train_generator,
                      steps_per_epoch=100,
                      epochs=80,
                      validation_data=validation_generator,
                      validation_steps=100)

PlotGraph(history)

#Frozen vgg16 model with data agumentation
#Frozen Vgg16 with data agumentation
network = VGG16FrozenNetwork()
train_datagen,test_datagen = GetAgumentedDatagen()  
train_generator = train_datagen.flow_from_directory(
        train_dir,
        target_size=(150,150),
        batch_size=20,
        class_mode='binary')

validation_generator = test_datagen.flow_from_directory(
        validation_dir,
        target_size=(150,150),
        batch_size=20,
        class_mode='binary')

history = network.fit_generator(train_generator,
                      steps_per_epoch=100,
                      epochs=80,
                      validation_data=validation_generator,
                      validation_steps=100)

network.save('Cats&Dogs_VGG16FrozenWithAugmentation.h5')
PlotGraph(history)


'''
i =0
for batch in datagen.flow(x,batch_size=1):
    plt.figure(i)
    imgplot = plt.imshow(image.array_to_img(batch[0]))
    i = i+1
    if i % 4 == 0:
        break

plt.show()
'''